from django.db import models


# a searchquery view to hold the name of a query
class SearchQuery(models.Model):
    term = models.CharField(max_length = 200)


# holds images for each search query, utilises foreign key to maintain images for each query
class Img(models.Model):
    # self explanatory field names
    search_query = models.ForeignKey(SearchQuery, on_delete=models.CASCADE)
    img_url = models.CharField(max_length=200)
    thumb_url = models.CharField(max_length=200)
    uses = models.IntegerField(default=0)
    img_id = models.IntegerField(default=0)
